<?php
namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $create = $auth->createPermission('create');
        $create->description = 'Create Items';
        $auth->add($create);

        $adminRole = $auth->createRole('admin');
        $adminRole->description = 'Admin role';
        $auth->add($adminRole);

        $auth->addChild($adminRole, $create);

        $update = $auth->createPermission('update');
        $update->description = 'Update Items';
        $auth->add($update);
        $auth->addChild($adminRole, $update);

        $delete = $auth->createPermission('delete');
        $delete->description = 'Delete Items';
        $auth->add($delete);
        $auth->addChild($adminRole, $delete);

        $view = $auth->createPermission('view');
        $view->description = 'View Items';
        $auth->add($view);
        $auth->addChild($adminRole, $view);

        $userRole = $auth->createRole('user');
        $userRole->description = 'User role';
        $auth->add($userRole);
        $auth->addChild($adminRole, $userRole);
        $auth->addChild($userRole, $view);

        $this->assignUsers();
    }

    private function assignUsers()
    {
        foreach (User::find()->all() as $user) {
            $user->delete();
        }

        $admin = new User();
        $admin->setAttributes([
            'username' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'email' => 'partyzan65@gmail.com',
            'access_token' => Yii::$app->security->generateRandomString(),
            'auth_key' => Yii::$app->security->generateRandomString(),
        ]);
        $admin->save(false);

        $auth = Yii::$app->authManager;
        $auth->assign($auth->getRole('admin'), $admin->getId());

        $user = new User();
        $user->setAttributes([
            'username' => 'demo',
            'password' => Yii::$app->security->generatePasswordHash('demo'),
            'email' => 'partyzan-lipetsk@yandex.ru',
            'access_token' => Yii::$app->security->generateRandomString(),
            'auth_key' => Yii::$app->security->generateRandomString(),
        ]);
        $user->save(false);

        $auth->assign($auth->getRole('user'), $user->getId());
    }
}