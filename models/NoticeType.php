<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notice_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $context
 *
 * @property Notice[] $notices
 */
class NoticeType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'context'], 'required'],
            [['name', 'context'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'context' => Yii::t('app', 'Context'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotices()
    {
        return $this->hasMany(Notice::className(), ['notice_type_id' => 'id']);
    }
}
