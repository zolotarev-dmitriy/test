<?php

namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $confirm;

    /**
     * @var string
     */
    public $email;

    /**
     * @var User
     */
    private $user;

    /**
     * RegisterForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->user = new User();
        $this->user->setScenario('register');
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password', 'confirm', 'email'], 'required'],
            [['confirm'], 'isConfirmPassword'],
            [['username'], 'validateUserName'],
            [['email'], 'email'],
            [['email'], 'validateEmail'],
        ];
    }

    /**
     * Проверка пароля и повотора пароля
     * @param $attribute
     * @param $params
     */
    public function isConfirmPassword($attribute, $params) {
        if ($this->password != $this->confirm) {
            $this->addError($attribute, 'Password and Confirm password must be identical');
        }
    }

    /**
     * Валидация имени пользователя на уникальность
     * @param $attribute
     * @param $params
     */
    public function validateUserName($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->user->username = $this->username;
            if (!$this->user->validate('username')) {
                $this->addErrors($this->user->getErrors());
            }
        }
    }

    /**
     * Проверка емайла пользователя на уникальность и валидность
     * @param $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->user->email = $this->email;
            if (!$this->user->validate('email')) {
                $this->addErrors($this->user->getErrors());
            }
        }
    }

    /**
     * Сохранение пользователя в БД
     * @return bool
     * @throws \yii\base\Exception
     */
    public function register() {
        if ($this->validate()) {
            $this->user->setAttributes([
                'username' => $this->username,
                'password' => Yii::$app->security->generatePasswordHash($this->password),
                'email' => $this->email,
                'auth_key' => Yii::$app->security->generateRandomString(),
                'access_token' => Yii::$app->security->generateRandomString()
            ]);

            $this->user->save(false);

            $auth = Yii::$app->authManager;
            $auth->assign($auth->getRole('user'), $this->user->getId());

            $notice = new Notice();



            $notice->setAttributes([
                ''
            ]);

            return true;
        }

        return false;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }
}